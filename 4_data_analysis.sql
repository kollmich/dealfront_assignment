

with base as (
	SELECT id, 
		enterprise_number, 
		first_name, 
		last_name, 
		street_name, 
		street_number, 
		zip, 
		country_code, 
		--date_format(from_unixtime(time_created), '%Y-%m-%d %h:%i:%s') as timecreated,
		--date_format(from_unixtime(time_modified), '%Y-%m-%d %h:%i:%s') as timemodified,
		--date_format(from_unixtime(time_updated), '%Y-%m-%d %h:%i:%s') as timeupdated,
	
		--cast(date_parse(date_format(from_unixtime(time_created), '%Y-%m-%d %h:%i:%s'), '%Y-%m-%d %h:%i:%s') as timestamp) as timecreated,
		--cast(date_parse(date_format(from_unixtime(time_modified), '%Y-%m-%d %h:%i:%s'), '%Y-%m-%d %h:%i:%s') as timestamp)  timemodified,
		--cast(date_parse(date_format(from_unixtime(time_updated), '%Y-%m-%d %h:%i:%s'), '%Y-%m-%d %h:%i:%s') as timestamp)  timeupdated,
		version, 
		sha1_hash
	FROM michal_kollar.dlfrt_kb_officer
),


-- a. Which company is related to the most officer records and how many are those? 
-- ANSWER: company with enterprise_number 429.053.863 with 67 records

records_per_company as (
	select enterprise_number
		,count(distinct id) record_count
	from base 
	group by 1
	order by 2 desc
)

-- b. How many officers are on average related to a company?
-- ANSWER: almost 2 officers per company, precisely 1.9234463276836158

--select avg(record_count)
--from records_per_company

-- c. Create a meaningful histogram that visualizes the distribution of number of related officers to companies
-- ANSWER:
--1	2258
--2	790
--3	237
--4	83
--5	39
--6	28
--7	17
--8	10
--9	9
--10	7
--11	9
--12	9
--13	5
--14	7
--15	6
--16	3
--18	5
--19	2
--20	1
--21	1
--22	1
--23	1
--24	1
--25	1
--29	1
--33	2
--35	2
--42	1
--52	1
--58	1
--60	1
--67	1

--select record_count
--	,count(enterprise_number) companies
--from records_per_company
--group by 1
--order by 1

-- d. How many of the provided records would you deem fit for further processing and actual use in Dealfront-Systems and why? 
-- Missing names
-- Names with only beginning letter and a dot, otherwise could be 1 character name?
-- Names with additional text in brackets (could be removed by regex)
-- Names with odd characters - wildcard can't be identified
-- Names with different format (big cap)
-- Records that have missing address
-- 4 values had a line break


--6809 rows in total
--159 missing names
--31 contain only name abbreviations
--all records are within reasonable update date
--52 are missing ZIP code
--I would use 6619 records, all except the one missing names or only their abbreviations





