from ydata_profiling import ProfileReport, compare
import pandas as pd

#Load in the data, 10000 rows each
vendor1_df = pd.read_csv('s3://pdw-users/personal/michal_kollar/archive/dfrt/vendor1_companies.csv', sep=';', on_bad_lines='skip')
print(vendor1_df.shape)
vendor1_report = ProfileReport(vendor1_df, title="Vendor 1")

vendor2_df = pd.read_csv('s3://pdw-users/personal/michal_kollar/archive/dfrt/vendor2_companies.csv', sep=';', on_bad_lines='skip')
print(vendor2_df.shape)
vendor2_report = ProfileReport(vendor2_df, title="Vendor 2")

vendor1_report.to_file("vendor1_report.html")
vendor2_report.to_file("vendor2_report.html")